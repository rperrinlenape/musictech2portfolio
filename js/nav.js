var navOpen = false;

$(document).ready(function () {
    $(".navButton").click(function () {
        if (navOpen === true) {
            $(".navLinks").css({'margin-top': '-400px'});
            navOpen = false;
        } else {
            $(".navLinks").css({'margin-top': '10px'});
            navOpen = true;
        }
    });
});